import torch
import torch.nn as nn

class block(nn.Module):
    def __init__(self,inChannel,intermediate_channel,identitymapping=None,stride=1):
        super().__init__()
        self.conv1= nn.Sequential(
            nn.Conv2d(inChannel,intermediate_channel,kernel_size=1,padding=0,stride=1),
            nn.BatchNorm2d(intermediate_channel),
            nn.ReLU()
        )
        self.conv2= nn.Sequential(
            nn.Conv2d(intermediate_channel, intermediate_channel, kernel_size=3, padding=1, stride=stride),
            nn.BatchNorm2d(intermediate_channel),
            nn.ReLU()
        )
        self.conv3 = nn.Sequential(
            nn.Conv2d(intermediate_channel, intermediate_channel * 4, kernel_size=1, padding=0, stride=1),
            nn.BatchNorm2d(intermediate_channel * 4),
        )
        self.relu= nn.ReLU()
        self.idn= identitymapping
    def forward(self,x):
        identity = x.clone() # x.clone in torch == copy.deepcopy()
        x= self.conv1(x)
        x=self.conv2(x)
        x=self.conv3(x)
        if self.idn is not None:
            identity= self.idn(identity)
        x += identity
        x= self.relu(x)
        return x

class ResNet(nn.Module):
    def __init__(self,block,layers,img_channel=3,num_class=1000):
        super().__init__()
        self.inChannel=64
        self.preblock= nn.Sequential(
            nn.Conv2d(img_channel,self.inChannel,kernel_size=7,stride=2,padding=3,bias=False),
            nn.BatchNorm2d(self.inChannel),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=3,stride=2,padding=1)
        )
        #########Block
        self.layer1= self._maker_layer(block,layers[0],64,1)
        self.layer2= self._maker_layer(block,layers[1],128,2)
        self.layer3= self._maker_layer(block,layers[2],256,2)
        self.layer4= self._maker_layer(block,layers[2],512,2)
        ########
        self.avg= nn.AdaptiveAvgPool2d((1,1))
        self.fc= nn.Linear(512 * 4, num_class)
    def _maker_layer(self,block,num_residual_block,intermediateChannel,stride):
        identity_mapping= None
        layers= []
        if stride!=1 or self.inChannel != intermediateChannel * 4:
            identity_mapping= nn.Sequential(
                nn.Conv2d(self.inChannel,intermediateChannel * 4,kernel_size=1,stride=stride,bias=False),
                nn.BatchNorm2d(intermediateChannel * 4)
            )
        layers.append(block(self.inChannel,intermediateChannel,identity_mapping,stride))
        self.inChannel= intermediateChannel * 4
        for i in range(num_residual_block-1):
            layers.append(block(self.inChannel,intermediateChannel))
        return  nn.Sequential(*layers)

    def forward(self,x):
        x= self.preblock(x)
        x= self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x= self.avg(x)
        x= x.reshape(x.shape[0],-1)
        x= self.fc(x)
        return x
def ResNet50(img_channel=3, num_classes=1000):
    return ResNet(block, [3, 4, 6, 3], img_channel, num_classes)


def ResNet101(img_channel=3, num_classes=1000):
    return ResNet(block, [3, 4, 23, 3], img_channel, num_classes)


def ResNet152(img_channel=3, num_classes=1000):
    return ResNet(block, [3, 8, 36, 3], img_channel, num_classes)

def test():
    BATCH_SIZE = 4
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    net = ResNet50(img_channel=3, num_classes=1000).to(device)
    y = net(torch.randn(BATCH_SIZE, 3, 224, 224).to(device)).to(device)
    assert y.size() == torch.Size([BATCH_SIZE, 1000])
    print(y.size())


if __name__ == "__main__":
    test()