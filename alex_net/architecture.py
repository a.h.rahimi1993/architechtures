import torch
import torch.nn as nn
import torchvision

import torch.optim as optim
from torch.optim import lr_scheduler

from torchvision import datasets, transforms

import numpy as np
import matplotlib.pyplot as plt
import time
import os
from tempfile import TemporaryDirectory
class AlexNet(nn.Module):
    def __init__(self,num_class=10):
        super().__init__()
        self.conv1= nn.Sequential(
            nn.Conv2d(in_channels=3,out_channels=96,kernel_size=11,stride=4,padding=0),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=3,stride=2)
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(in_channels=96, out_channels=256, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=3, stride=2)
        )
        self.conv3 = nn.Sequential(
            nn.Conv2d(in_channels=256, out_channels=384, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
        )
        self.conv4 = nn.Sequential(
            nn.Conv2d(in_channels=384, out_channels=384, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
        )
        self.conv5 = nn.Sequential(
            nn.Conv2d(in_channels=384, out_channels=256, kernel_size=3, stride=1, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=3, stride=2)
        )
        self.fc1= nn.Sequential(
            nn.Dropout(0.5),
            nn.Linear(9216,4096),
            nn.ReLU()
        )
        self.fc2 = nn.Sequential(
            nn.Dropout(0.5),
            nn.Linear(4096, 4096),
            nn.ReLU()
        )
        self.fc3 = nn.Sequential(
            nn.Dropout(0.5),
            nn.Linear(4096, num_class),
            nn.ReLU()
        )
    def forward(self,x):
        out= self.conv1(x)
        out= self.conv2(out)
        out= self.conv3(out)
        #print(out.shape)
        out= self.conv4(out)
        out= self.conv5(out)
        out= out.reshape(out.size(0),-1)
        out = self.fc1(out)
        out= self.fc2(out)
        out= self.fc3(out)
        return out
def train(model,criterion,optim,lr_s,num_epoch=10):
    since= time.time()
    with TemporaryDirectory() as temdir:
        best_model_param_path= os.path.join(temdir,"best_model_params.pt")
        torch.save(model.state_dict(),best_model_param_path)
        best_acc= 0.0
        for epoch in range(num_epoch):
            print("Epoch {0} is started ...".format(epoch))
            print("-"*25)
            for phase in ["train","test"]:
                if phase == "train":
                    model.train()
                else:
                    model.eval()
                running_loss= 0.0
                running_corrects= 0
                for inputs, labels in dataloaders[phase]:
                    inputs = inputs.to(device)
                    labels = labels.to(device)
                    optim.zero_grad()
                    with torch.set_grad_enabled(phase == "train"):
                        outputs = model(inputs)
                        _,preds = torch.max(outputs,1)
                        loss= criterion(outputs,labels)
                        if phase == "train":
                            loss.backward()
                            optim.step()
                    running_loss += loss.item() * inputs.size(0)
                    running_corrects += torch.sum(preds == labels.data)
                if phase == "train":
                    lr_s.step()
                epoch_loss = running_loss/dataset_size[phase]
                epoch_acc= running_corrects.double()/dataset_size[phase]
                print(f"Phase {phase}: \n   Loss : {epoch_loss} \n   epoch accuracy: {epoch_acc}")
                if phase == "test" and epoch_acc > best_acc:
                    best_acc= epoch_acc
                    torch.save(model.state_dict(),best_model_param_path)
                print()
        total_running_time= time.time() - since
        print(f"Traing is completed in {total_running_time}\n The best accuracy is {best_acc} on test")
        model.load_state_dict(torch.load(best_model_param_path))
        return model
def imshow(imgs,title="some img"):
    imgs= imgs.numpy().transpose((1,2,0))
    mean = np.array([0.485,0.456,0.406])
    std= np.array([0.229,0.224,0.225])
    imgs= std * imgs + mean
    imgs= np.clip(imgs,0,1)
    plt.imshow(imgs)
    plt.pause(3)
if __name__ == '__main__':
    #Reference : https://pytorch.org/tutorials/beginner/transfer_learning_tutorial.html?highlight=transfer%20learning%20ant%20bees
    Batch_size= 15
    device= "cuda" if torch.cuda.is_available() else "cpu"
    print(device)
    data_transform= {
        "train": transforms.Compose([
            transforms.RandomResizedCrop(227),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize([0.485,0.456,0.406],[0.229,0.224,0.225])
        ]),
        "test" : transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(227),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])
    }
    data_dir= "../../dataset/cifar_10_img"
    img_dataset= {x: datasets.ImageFolder(os.path.join(data_dir,x),data_transform[x]) for x in ["train","test"]}
    dataloaders= {x: torch.utils.data.DataLoader(img_dataset[x],batch_size=32,num_workers=4,shuffle=True) for x in ["train","test"]}#(227,227,3) ->tensor -> (3,227,227) -> batch_size=32 -> (32,3,227,227)
    dataset_size= {x: len(img_dataset[x]) for x in ["train","test"]}
    print(dataset_size)
    print(img_dataset["train"].classes)
    inputs, classes= next(iter(dataloaders["train"]))
    out= torchvision.utils.make_grid(inputs)
    #imshow(out,title=[classes[x] for x in classes])
    model = AlexNet(2).to(device)
    criteration = nn.CrossEntropyLoss()
    optim = optim.SGD(model.parameters(),lr=0.001,momentum=0.9)
    lr_s= lr_scheduler.StepLR(optim,step_size=7,gamma=0.1)
    proper_model= train(model,criteration,optim,lr_s,num_epoch=28)