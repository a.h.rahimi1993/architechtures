import torch
import torch.nn as nn

VGG_types = {#RGB = 3 Channel
    "VGG11":[64,"M",128,"M",256,256,"M",512,512,"M",512,512,"M"],
    "VGG13":[64,64,"M",128,128,"M",256,256,"M",512,512,"M",512,512,"M"],
    "VGG16":[64,64,"M",128,128,"M",256,256,256,"M",512,512,512,"M",512,512,512,"M"],
    "VGG19":[64,64,"M",128,128,"M",256,256,256,256,"M",512,512,512,512,"M",512,512,512,512,"M"],
    }
# Convlayer -> kernel is (3,3) and padding and stride are (1,1)
# MaxPool -> kernel and stride both are (2,2)
# Always fully connected layer is the same (512 * 7 * 7) -> 4096 -> 4096 -> 1000
# Input image is 224

class VGGnet(nn.Module):

    def __init__(self,inChannel=3, numClass=1000):
        super(VGGnet,self).__init__()
        self.inchannel= inChannel
        self.numClass= numClass
        self.convLayer= self.__create_conv(VGG_types["VGG16"])
        self.fc= nn.Sequential(
            nn.Linear(512*7*7,4096),
            nn.ReLU(),
            nn.Dropout(p=0.5),
            nn.Linear(4096,4096),
            nn.ReLU(),
            nn.Dropout(p=0.5),
            nn.Linear(4096,self.numClass),
        )
    def __create_conv(self,architecture):
        layers= [] # [1,2,3] + [4,5,6] = [1,2,3,4,5,6]
        inchannel =self.inchannel

        for a in architecture:
            if type(a) == int:
                outChannel= a
                layers += [nn.Conv2d(inchannel,outChannel,(3,3),stride=(1,1), padding=(1,1)),
                           nn.ReLU(),
                           ]
                inchannel= outChannel
            else:
                layers +=[nn.MaxPool2d(kernel_size=(2,2),stride=(2,2))]
        return nn.Sequential(*layers)
    def forward(self,x): #x = [BatchSize,3,244,244]
        x= self.convLayer(x)
        # batchSize * 512 * 7 * 7
        x= x.reshape(x.shape[0],-1)
        # Now the size is batchSize , 25088
        return self.fc(x)

if __name__ == '__main__':
    device = "cuda" if torch.cuda.is_available() else "cpu"
    vgg = VGGnet(3,1000).to(device)
    batchsize= 4
    my_random_inputs= torch.rand((4,3,224,244)).to(device)
    print(vgg(my_random_inputs).shape)